# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: index.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='index.proto',
  package='tutorial',
  syntax='proto3',
  serialized_pb=_b('\n\x0bindex.proto\x12\x08tutorial\":\n\x05Index\x12\x0e\n\x06\x66older\x18\x01 \x01(\t\x12!\n\x05\x66iles\x18\x02 \x03(\x0b\x32\x12.tutorial.FileInfo\"@\n\x0bIndexUpdate\x12\x0e\n\x06\x66older\x18\x01 \x01(\t\x12!\n\x05\x66iles\x18\x02 \x03(\x0b\x32\x12.tutorial.FileInfo\"\xcb\x02\n\x08\x46ileInfo\x12\x0c\n\x04name\x18\x01 \x01(\t\x12$\n\x04type\x18\x02 \x01(\x0e\x32\x16.tutorial.FileInfoType\x12\x0c\n\x04size\x18\x03 \x01(\x03\x12\x13\n\x0bpermissions\x18\x04 \x01(\r\x12\x12\n\nmodified_s\x18\x05 \x01(\x03\x12\x13\n\x0bmodified_ns\x18\x0b \x01(\x05\x12\x13\n\x0bmodified_by\x18\x0c \x01(\x04\x12\x0f\n\x07\x64\x65leted\x18\x06 \x01(\x08\x12\x0f\n\x07invalid\x18\x07 \x01(\x08\x12\x16\n\x0eno_permissions\x18\x08 \x01(\x08\x12!\n\x07version\x18\t \x01(\x0b\x32\x10.tutorial.Vector\x12\x10\n\x08sequence\x18\n \x01(\x03\x12#\n\x06\x42locks\x18\x10 \x03(\x0b\x32\x13.tutorial.BlockInfo\x12\x16\n\x0esymlink_target\x18\x11 \x01(\t\"7\n\tBlockInfo\x12\x0e\n\x06offset\x18\x01 \x01(\x03\x12\x0c\n\x04size\x18\x02 \x01(\x05\x12\x0c\n\x04hash\x18\x03 \x01(\x0c\"-\n\x06Vector\x12#\n\x08\x63ounters\x18\x01 \x03(\x0b\x32\x11.tutorial.Counter\"$\n\x07\x43ounter\x12\n\n\x02id\x18\x01 \x01(\x04\x12\r\n\x05value\x18\x02 \x01(\x04*e\n\x0c\x46ileInfoType\x12\x08\n\x04\x46ILE\x10\x00\x12\r\n\tDIRECTORY\x10\x01\x12\x14\n\x0cSYMLINK_FILE\x10\x02\x1a\x02\x08\x01\x12\x19\n\x11SYMLINK_DIRECTORY\x10\x03\x1a\x02\x08\x01\x12\x0b\n\x07SYMLINK\x10\x04\x62\x06proto3')
)

_FILEINFOTYPE = _descriptor.EnumDescriptor(
  name='FileInfoType',
  full_name='tutorial.FileInfoType',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='FILE', index=0, number=0,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='DIRECTORY', index=1, number=1,
      options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='SYMLINK_FILE', index=2, number=2,
      options=_descriptor._ParseOptions(descriptor_pb2.EnumValueOptions(), _b('\010\001')),
      type=None),
    _descriptor.EnumValueDescriptor(
      name='SYMLINK_DIRECTORY', index=3, number=3,
      options=_descriptor._ParseOptions(descriptor_pb2.EnumValueOptions(), _b('\010\001')),
      type=None),
    _descriptor.EnumValueDescriptor(
      name='SYMLINK', index=4, number=4,
      options=None,
      type=None),
  ],
  containing_type=None,
  options=None,
  serialized_start=627,
  serialized_end=728,
)
_sym_db.RegisterEnumDescriptor(_FILEINFOTYPE)

FileInfoType = enum_type_wrapper.EnumTypeWrapper(_FILEINFOTYPE)
FILE = 0
DIRECTORY = 1
SYMLINK_FILE = 2
SYMLINK_DIRECTORY = 3
SYMLINK = 4



_INDEX = _descriptor.Descriptor(
  name='Index',
  full_name='tutorial.Index',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='folder', full_name='tutorial.Index.folder', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='files', full_name='tutorial.Index.files', index=1,
      number=2, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=25,
  serialized_end=83,
)


_INDEXUPDATE = _descriptor.Descriptor(
  name='IndexUpdate',
  full_name='tutorial.IndexUpdate',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='folder', full_name='tutorial.IndexUpdate.folder', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='files', full_name='tutorial.IndexUpdate.files', index=1,
      number=2, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=85,
  serialized_end=149,
)


_FILEINFO = _descriptor.Descriptor(
  name='FileInfo',
  full_name='tutorial.FileInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='name', full_name='tutorial.FileInfo.name', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='type', full_name='tutorial.FileInfo.type', index=1,
      number=2, type=14, cpp_type=8, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='size', full_name='tutorial.FileInfo.size', index=2,
      number=3, type=3, cpp_type=2, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='permissions', full_name='tutorial.FileInfo.permissions', index=3,
      number=4, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='modified_s', full_name='tutorial.FileInfo.modified_s', index=4,
      number=5, type=3, cpp_type=2, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='modified_ns', full_name='tutorial.FileInfo.modified_ns', index=5,
      number=11, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='modified_by', full_name='tutorial.FileInfo.modified_by', index=6,
      number=12, type=4, cpp_type=4, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='deleted', full_name='tutorial.FileInfo.deleted', index=7,
      number=6, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='invalid', full_name='tutorial.FileInfo.invalid', index=8,
      number=7, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='no_permissions', full_name='tutorial.FileInfo.no_permissions', index=9,
      number=8, type=8, cpp_type=7, label=1,
      has_default_value=False, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='version', full_name='tutorial.FileInfo.version', index=10,
      number=9, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='sequence', full_name='tutorial.FileInfo.sequence', index=11,
      number=10, type=3, cpp_type=2, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='Blocks', full_name='tutorial.FileInfo.Blocks', index=12,
      number=16, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='symlink_target', full_name='tutorial.FileInfo.symlink_target', index=13,
      number=17, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=152,
  serialized_end=483,
)


_BLOCKINFO = _descriptor.Descriptor(
  name='BlockInfo',
  full_name='tutorial.BlockInfo',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='offset', full_name='tutorial.BlockInfo.offset', index=0,
      number=1, type=3, cpp_type=2, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='size', full_name='tutorial.BlockInfo.size', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='hash', full_name='tutorial.BlockInfo.hash', index=2,
      number=3, type=12, cpp_type=9, label=1,
      has_default_value=False, default_value=_b(""),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=485,
  serialized_end=540,
)


_VECTOR = _descriptor.Descriptor(
  name='Vector',
  full_name='tutorial.Vector',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='counters', full_name='tutorial.Vector.counters', index=0,
      number=1, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=542,
  serialized_end=587,
)


_COUNTER = _descriptor.Descriptor(
  name='Counter',
  full_name='tutorial.Counter',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='id', full_name='tutorial.Counter.id', index=0,
      number=1, type=4, cpp_type=4, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='value', full_name='tutorial.Counter.value', index=1,
      number=2, type=4, cpp_type=4, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=589,
  serialized_end=625,
)

_INDEX.fields_by_name['files'].message_type = _FILEINFO
_INDEXUPDATE.fields_by_name['files'].message_type = _FILEINFO
_FILEINFO.fields_by_name['type'].enum_type = _FILEINFOTYPE
_FILEINFO.fields_by_name['version'].message_type = _VECTOR
_FILEINFO.fields_by_name['Blocks'].message_type = _BLOCKINFO
_VECTOR.fields_by_name['counters'].message_type = _COUNTER
DESCRIPTOR.message_types_by_name['Index'] = _INDEX
DESCRIPTOR.message_types_by_name['IndexUpdate'] = _INDEXUPDATE
DESCRIPTOR.message_types_by_name['FileInfo'] = _FILEINFO
DESCRIPTOR.message_types_by_name['BlockInfo'] = _BLOCKINFO
DESCRIPTOR.message_types_by_name['Vector'] = _VECTOR
DESCRIPTOR.message_types_by_name['Counter'] = _COUNTER
DESCRIPTOR.enum_types_by_name['FileInfoType'] = _FILEINFOTYPE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

Index = _reflection.GeneratedProtocolMessageType('Index', (_message.Message,), dict(
  DESCRIPTOR = _INDEX,
  __module__ = 'index_pb2'
  # @@protoc_insertion_point(class_scope:tutorial.Index)
  ))
_sym_db.RegisterMessage(Index)

IndexUpdate = _reflection.GeneratedProtocolMessageType('IndexUpdate', (_message.Message,), dict(
  DESCRIPTOR = _INDEXUPDATE,
  __module__ = 'index_pb2'
  # @@protoc_insertion_point(class_scope:tutorial.IndexUpdate)
  ))
_sym_db.RegisterMessage(IndexUpdate)

FileInfo = _reflection.GeneratedProtocolMessageType('FileInfo', (_message.Message,), dict(
  DESCRIPTOR = _FILEINFO,
  __module__ = 'index_pb2'
  # @@protoc_insertion_point(class_scope:tutorial.FileInfo)
  ))
_sym_db.RegisterMessage(FileInfo)

BlockInfo = _reflection.GeneratedProtocolMessageType('BlockInfo', (_message.Message,), dict(
  DESCRIPTOR = _BLOCKINFO,
  __module__ = 'index_pb2'
  # @@protoc_insertion_point(class_scope:tutorial.BlockInfo)
  ))
_sym_db.RegisterMessage(BlockInfo)

Vector = _reflection.GeneratedProtocolMessageType('Vector', (_message.Message,), dict(
  DESCRIPTOR = _VECTOR,
  __module__ = 'index_pb2'
  # @@protoc_insertion_point(class_scope:tutorial.Vector)
  ))
_sym_db.RegisterMessage(Vector)

Counter = _reflection.GeneratedProtocolMessageType('Counter', (_message.Message,), dict(
  DESCRIPTOR = _COUNTER,
  __module__ = 'index_pb2'
  # @@protoc_insertion_point(class_scope:tutorial.Counter)
  ))
_sym_db.RegisterMessage(Counter)


_FILEINFOTYPE.values_by_name["SYMLINK_FILE"].has_options = True
_FILEINFOTYPE.values_by_name["SYMLINK_FILE"]._options = _descriptor._ParseOptions(descriptor_pb2.EnumValueOptions(), _b('\010\001'))
_FILEINFOTYPE.values_by_name["SYMLINK_DIRECTORY"].has_options = True
_FILEINFOTYPE.values_by_name["SYMLINK_DIRECTORY"]._options = _descriptor._ParseOptions(descriptor_pb2.EnumValueOptions(), _b('\010\001'))
# @@protoc_insertion_point(module_scope)
