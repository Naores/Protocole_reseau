#

import struct
import socket
import os
import sys
from datetime import datetime
import hello_pb2
import lz4
from hashlib import sha256
import ssl
from util import *
from device_id import *
from config import *

#INTIALISATION
class State:
    def __init__(self, ssl_cert_file):
        self.messageID = 0
        self.compression = 0

        self.protocolVersion = 0

        self.clientName    = b"SyncThingPy"
        self.clientVersion = b"v0.0.1"

        self.folder_base = ''
        self.registered_folders = []

        self.stage = 0

        with open(ssl_cert_file, 'r') as f:
            v = ssl.PEM_cert_to_DER_cert(f.read())
            self.deviceID = sha256(v).digest()
            # print('My DeviceID: %s' % get_device_id(self.deviceID))

    def bumpID(self):
        self.messageID += 1
        if self.messageID > 0xfff:
            self.messageID = 0

#PROCESS MESSAGES
class MessageProcessor:

    def __init__(self, state, socket):
        self.state = state
        self.socket = socket
        self.server_down = 0

    def send_greetings(self):
        self.receive_message()


    def receive_message(self):
        if not os.path.exists("sync"):
            os.makedirs("sync")
        os.chdir("./sync/")
        i=1
        content=[]
        msgheader = recv_hello(self.socket, 8)
        send_hello(self.socket)
        fl,fl2=recv_cluster(self.socket)
        send_cluster(self.socket,fl,fl2)
        index1=recv_index(self.socket)
        index2=recv_index(self.socket)
        i,content=send_request(self.socket,index1,i,content)
        i,content=send_request(self.socket,index2,i,content)
        return 


#MAIN
if __name__ == "__main__":
    state = State(ssl_cert_file)
    state.folder_base = 'sync'

    if not server_address:
        server_address = announcement(state.deviceID, get_device_id_from_string(server_deviceid))

        if server_address:
            print('Server found: %s:%s' % server_address)
        else:
            print('Server not found')
            sys.exit(1)

    listen_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    listen_sock.settimeout(20)

    listen_sock = ssl.wrap_socket(listen_sock,
                                  keyfile=ssl_key_file,
                                  certfile=ssl_cert_file,
                                  ssl_version=ssl.PROTOCOL_TLSv1_2)

    listen_sock.connect(server_address)

    messageProcessor = MessageProcessor(state, socket)

    msgProc = MessageProcessor(state, listen_sock)
    msgProc.send_greetings()

