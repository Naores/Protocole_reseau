import os
from datetime import datetime
from array import array
import hello_pb2
import header_pb2
import cluster_pb2
import index_pb2
import request_pb2
import response_pb2
import lz4
import struct
import socket
import pathlib

def bytearray2str(s):
    return "".join([chr(a) for a in s])

def recv_hello(socket, MSGLEN):
    hello=hello_pb2.Hello()
    bytes_recd = 0
    remaining = MSGLEN
    while remaining > 0:
        test = socket.recv()
        length = int.from_bytes(test[4:6], byteorder='big')
        msg=hello.ParseFromString(test[6:])
        remaining -= len(test) 
    return hello
	
def recv_header(socket):
    header=header_pb2.Header()
    bytes_recd = 0
    remaining = 1
    while remaining > 0:
        test = socket.recv(2)
        length = int.from_bytes(test[0:2], byteorder='big')
        msg=header.ParseFromString(test[0:length])
        remaining -= len(test) 
    return length

def recv_cluster(socket):
    length=recv_header(socket)
    cluster=cluster_pb2.ClusterConfig()
    data = socket.recv(4)
    msg = bytearray()
    bytes_recd = 0
    mlength = int.from_bytes(data[0:4], byteorder='big')
    remaining = mlength
    while remaining > 0:
        chunk = socket.recv(min(mlength - bytes_recd, 2048))
        msg.extend(chunk)
        bytes_recd =+ len(chunk)
        remaining -= len(chunk)
    m=cluster.ParseFromString(msg)
    name=cluster.folders[0].id
    name2=cluster.folders[1].id
    return name,name2
	
def recv_index(socket):
    length=recv_header(socket)
    index=index_pb2.Index()
    data = socket.recv(6)
    msg = bytearray()
    bytes_recd = 0
    mlength = int.from_bytes(data[2:6], byteorder='big')
    remaining = mlength
    while remaining > 0:
        chunk = socket.recv(min(mlength - bytes_recd, 2048))
        msg.extend(chunk)
        bytes_recd =+ len(chunk)
        remaining -= len(chunk)
    ds=len(msg)
    m=index.ParseFromString(msg)
    return (index)

def recv_response(socket,content,index):
    length=recv_header(socket)
    resp=response_pb2.Response()
    data = socket.recv(6)
    msg = bytearray()
    bytes_recd = 0
    mlength = int.from_bytes(data[2:6], byteorder='big')
    remaining = mlength
    while remaining > 0:
        chunk = socket.recv(min(mlength - bytes_recd, 2048))
        msg.extend(chunk)
        bytes_recd =+ len(chunk)
        remaining -= len(chunk)
    m=resp.ParseFromString(msg)
    path=content[resp.id-1]
    if path.find("/") == -1:
        if path.find(".") == -1:
            if not os.path.exists(path):
                os.makedirs(path)
        else:
            if not os.path.exists(path):
                f= open(path,"wb")
                f.write(resp.data)
    else:
        nb=path.count('/')
        p = pathlib.Path(path)
        try:
            p.parent.mkdir(parents=True, exist_ok=True)
            os.chdir(p.parent)
            f= open(p.name,"wb")
            f.write(resp.data)
            for x in range(0,nb):
                os.chdir("..")
        except:
            print("not supported")
    return (resp)

def send_cluster(socket,fl,fl2):
    header=header_pb2.Header()
    header.type=0
    header.compression=0
    hd=header.SerializeToString()
    length=len(hd)
    cluster=cluster_pb2.ClusterConfig()
    folder=cluster.folders.add()
    folder.id=fl
    folder.read_only=1
    folder2=cluster.folders.add()
    folder2.id=fl2
    folder2.read_only=1
    msg=cluster.SerializeToString()
    length2=len(msg)
    packet = bytearray()
    packet=struct.pack('>H',length)
    packet+=hd
    packet2=struct.pack('>I',length2)
    packet+=packet2
    packet+=msg
    send_message(socket, packet)
    return 

def send_request(socket,index,i,content):
    header=header_pb2.Header()
    header.type=3
    header.compression=0
    hd=header.SerializeToString()
    length=len(hd)
    os.makedirs(index.folder)
    os.chdir("./"+index.folder)
    for file in index.files:
        req=request_pb2.Request()
        req.id=i
        req.folder=index.folder
        req.name=file.name
        req.offset=0
        req.size=file.size
        msg=req.SerializeToString()
        length2=len(msg)
        packet = bytearray()
        packet=struct.pack('>H',length)
        packet+=hd
        packet2=struct.pack('>I',length2)
        packet+=packet2
        packet+=msg
        content.insert(i,file.name)
        send_message(socket, packet)
        recv_response(socket,content,index)
        i=i+1
    os.chdir("..")
    return i,content

def send_hello(socket):
    Rhello=hello_pb2.Hello()
    Rhello.device_name="deillon"
    Rhello.client_name="laurent"
    Rhello.client_version="v0.14.43"
    msg=Rhello.SerializeToString()
    length=len(msg)
    packet=struct.pack('>IH',0X2EA7D90B,length)
    packet+=msg
    send_message(socket, packet)
    return 

def send_message(socket, msg):
    socket.sendall(msg)
  

